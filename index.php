<!doctype html>
<html lang="en">
 <?php session_start(); if(isset($_SESSION['nama'])) 
    header("location: dashboard.php");
    include 'koneksi.php';
  ?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
     
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center"><a href="../index.html"><img class="logo-img" width= "100px" height="100px" src="file/logi.png" alt="logo"></a>
                 <?php
                  include "koneksi.php";
    if (isset($_POST['submit'])) {
    $username        = $_POST['username'];
    $password    = $_POST['password'];
    //cek old password
    $query = "SELECT * FROM user WHERE nama='$username' AND pass='$password'";
    $sql = mysqli_query ($koneksi, $query);
    $has = mysqli_num_rows($sql);
    if (mysqli_num_rows($sql)>0) {
        session_start();
    while($data = mysqli_fetch_array($sql)){
    $_SESSION['nama'] = $data['nama'];
    $_SESSION['foto'] = $data['fotou'];
    $_SESSION['id'] = $data['id'];
    $_SESSION['pw'] = $data['pass'];
    $_SESSION['status'] = $data['level'];
}
        ?>
            <script language="JavaScript">
            document.location='dashboard.php';
            </script>

            <?php
        }
        else {echo "<span class='splash-description text-danger'>Username atau Password Salah</span></div>";}
    }
?>
            <div class="card-body">
                <form method="post" action="">
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="username" name="username" type="text" placeholder="Username" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="password" name="password" type="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox"><span class="custom-control-label" style="float: left;">Remember Me</span>
                        </label>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block view">Sign in</button>
                </form>
            </div>
            <div class="card-footer bg-white p-0  ">
                <div class="card-footer-item card-footer-item-bordered">
                    <a href="#" class="footer-link">Forgot Password</a>
                </div>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>
 
</html>
