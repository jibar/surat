<?php
    require_once "static.php";
?>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Edit Surat Kurikulum</h2>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
            <div class="row">
                <?php 
                    include 'koneksi.php';
                    $id = $_GET['id'];
                    $que = "select * from kkurikulum where id_kurikulum  = $id";
                    $hasil = mysqli_query($koneksi , $que);
                    while ($data = mysqli_fetch_array($hasil)){
                ?>
                        <!-- ============================================================== -->
                        <!-- basic form -->
                        <!-- ============================================================== -->
                        <div class="col-xl-1 col-lg-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Surat Keluar</h5>
                                <div class="card-body">
                                    <form method="post" action="upkel_kurikulum.php?id=<?php echo $id ?>" id="basicform" enct   ype="multipart/form-data" data-parsley-validate="">
                                        <div class="form-group">
                                            <label for="id_kurikulum">Id Surat</label>
                                            <input id="id_kurikulum" type="text" name="id_kurikulum" data-parsley-trigger="change" required="" value="<?php echo $data['id_kurikulum'] ?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_surat">Nama surat</label>
                                            <input id="nama_surat" type="text" name="nama_surat" data-parsley-trigger="change" required="" value="<?php echo $data['nama_surat'] ?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="pengirim">Pengirim</label>
                                            <input id="pengirim" type="text" name="pengirim" data-parsley-trigger="change" required="" value="<?php echo $data['pengirim'] ?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="penerima">Penerima</label>
                                            <input id="penerima" type="text" name="penerima" data-parsley-trigger="change" required="" value="<?php echo $data['penerima'] ?>" autocomplete="off" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label for="tanggal">tanggal</label>
                                            <input id="tanggal" type="date" name="tanggal" data-parsley-trigger="change" required="" value="<?php echo $data['tanggal'] ?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                                               
                                            </div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                    <button class="btn btn-space btn-secondary">Cancel</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end basic form -->
<?php } ?>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer bg-dark fixed-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                             SMA NEGERI 5 MATARAM
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>