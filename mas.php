<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
</head>

<body> 
    <?php include "koneksi.php"; 
    $id = $_GET['id'];
        $que ="select * from masuk where idm = $id";
        $hasil = mysqli_query($koneksi, $que);
        $data = $hasil->fetch_array();
    ?>
   <div class="card-body">
                        <form method="post" action="#" id="basicform" enctype="multipart/form-data" data-parsley-validate="">
                                        <div class="form-group">
                                            <label for="nosurat">Nomor Surat</label>
                                            <input id="nosurat" type="text" name="no" data-parsley-trigger="change" required="" value="<?php echo $data['nom']?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="pengirim">Pengirim</label>
                                            <input id="pengirim" type="text" name="pengirim" data-parsley-trigger="change" required=""value="<?php echo $data['pengirimm']?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="penerima">Penerima</label>
                                            <input id="penerima" type="text" name="penerima" data-parsley-trigger="change" required="" value="<?php echo $data['penerimam']?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl">Tanggal Terima</label>
                                            <input id="tgl" type="text" name="tgl" data-parsley-trigger="change" required="" value="<?php echo $data['tglm']?>" autocomplete="off" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label for="perihal">Perihal</label>
                                            <input id="perihal" type="text" name="perihal" data-parsley-trigger="change" required="" value="<?php echo $data['perihalm']?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="file">File</label>
                                            <input id="file" type="file" name="file" data-parsley-trigger="change" required="" value="<?php echo $data['filem']?>" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                                               
                                            </div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                    <button class="btn btn-space btn-secondary">Cancel</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/libs/js/script_cari.js"></script>
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>






