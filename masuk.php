<?php session_start();
    $stat = $_SESSION['status'] ;
    if ($stat =='admin')
        require_once "static.php";
    else
        require_once "static1.php";
?>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    
                     <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <?php
                    $batas=10; //satu halaman menampilkan 10 baris
                    $halaman=$_GET['halaman'];
                    $posisi=null;
                    if(empty($halaman)){
                      $posisi=0;
                      $halaman=1;
                    }else{
                      $posisi=($halaman-1)* $batas;
                    }
                     $pilih = "select * from masuk limit $posisi,$batas "; 
                          $hasil = mysqli_query($koneksi, $pilih);
                    ?>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                        <div class="card">
                            <nav class="navbar navbar-light bg-light justify-content-between">
                          <a class="navbar-brand">Surat Masuk</a>
                          <form class="form-inline" method="post" action="cetak.php">
                            <button class="btn-secondary pr-10"> Cetak</button>
                            <a> Cari </a>
                            <input class="form-control mr-sm-2" name="cari" id="cari" type="search" placeholder="Search" aria-label="Search">
                          </form>
                        </nav>
                        <div id="container">
                            <div class="card-body">  
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>id masuk</th>
                                                <th>nama surat</th>
                                                <th>pengirim</th>
                                                <th>penerima</th>
                                                <th>tanggal</th>
                                                <th>File</th>
                                                <?php if($_SESSION['status']=='admin') {?>
                                                <th colspan="2">Aksi</th>
                                            <?php }?>
                                            </tr>
                                        </thead>
                                        <tbody><?php 
                                            $no=1;
                                            while($data = mysqli_fetch_array($hasil)){
                                                ?>

                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $data["id_masuk"];?></td>
                                                <td><?php echo $data["nama_surat"];?></td>
                                                <td><?php echo $data["pengirim"];?></td>
                                                <td><?php echo $data["penerima"];?></td>
                                                <td><?php echo $data["tanggal"];?></td>
                                                <td><a href="file/<?php echo $data["filem"];?>"><img src="file/<?php echo $data["filem"];?>"></a></td>
                                                <?php if($_SESSION['status']=='admin') {?>
                                                <td><button type="button" class="btn-primary view_data" onclick="window.location.href='edit_masuk.php?id=<?php echo $data['id_masuk']; ?>'">
                                                  Edit
                                                </button></td>
                                                <td><a href="hapus.php?id=<?php echo $data['id_masuk']; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a></td>
                                                <?php }?>
                                            </tr>
                                            <?php $no++; } ?> 
                                            
                                    </table>
                                </div>
                            </div>
                             <?php    
     
// 4). code untuk Menampilkan nomor paging di bagian bawah tabel.
      $sql_paging = mysqli_query($koneksi, "select * from masuk");
      $jmldata = mysqli_num_rows($sql_paging);
      $jumlah_halaman = ceil($jmldata / $batas);
 
      echo "Halaman :";
      for($i = 1; $i <= $jumlah_halaman; $i++)
        if($i != $halaman) {
          echo "<a href=masuk.php?halaman=$i>$i</a>|";
        } else {
          echo "<b>$i</b>|";
        }
      ?>
    <br>
    Jumlah data :<?php echo $jmldata;?>
             
                        </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
            <?php
    require_once "footer.php";
?>          
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
                    

    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
      <script src="assets/libs/js/script_cari.js"></script>
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>