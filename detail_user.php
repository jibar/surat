<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
</head>

<body>
    <?php
  include 'koneksi.php';
    if($_GET['cari']) {
        // mengambil data berdasarkan id
        if ($_GET['cari']=="") {
            header("location: user.php?halaman=1") ;    
        }
            else {
        $sql = "SELECT * FROM user WHERE id LIKE '%".$_GET["cari"]."%' or nama LIKE '%".$_GET["cari"]."%' or alamat LIKE '%".$_GET["cari"]."%' or nohp LIKE '%".$_GET["cari"]."%' order by level ASC";
        }
        $hasil= mysqli_query($koneksi, $sql);
         if (mysqli_num_rows($hasil)>0){
       ?>
             <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>NO HP</th>
                                                <th>Level</th>
                                                <th>Foto</th>
                                                <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody><?php 
                                               
                                            $no=1;
                                            while($data = mysqli_fetch_array($hasil)){
                                                ?>
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $data["nama"];?></td>
                                                <td><?php echo $data["alamat"];?></td>
                                                <td><?php echo $data["nohp"];?></td>
                                                <td><?php echo $data["level"];?></td>
                                                <td><img src="file/<?php echo $data["fotou"];?>"></td>
                                                <td><button type="button" class="btn-primary" data-toggle="modal" data-target="#login">
                                                  Edit
                                                </button></td>
                                                <td><a href="hapusu.php?id=<?php echo $data["id"]; ?>">hapus</a></td>
                                            </tr>
                                            <?php $no++; } } else echo "<center><h3>Data Tidak Ditemukan</h3></center>"; ?> 
                                            
                                    </table>
                                </div>
                            </div>
                        </div>
        <?php 
 
        }
    $koneksi->close();
?>

                <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <div class="card-body">
                        <form method="post" action="#" id="basicform" enctype="multipart/form-data" data-parsley-validate="">
                                        
                                        <div class="form-group">
                                            <label for="nama_surat">Nama</label>
                                            <input id="nama" type="text" name="nama" data-parsley-trigger="change" required="" placeholder="Masukan nama surat" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="pengirim">alamat</label>
                                            <input id="alamat" type="text" name="alamat" data-parsley-trigger="change" required="" placeholder="Masukan nama pengirim" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nohp">NO HP</label>
                                            <input id="nohp" type="text" name="nohp" data-parsley-trigger="change" required="" placeholder="Masukan nama penerima" autocomplete="off" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label for="level">Jabatan</label>
                                                  <select id="level" name="level" class="form-control">
                                                    <option selected value=0>Pilih</option>
                                                    <option value="admin">Admin</option>
                                                    <option value="user">User</option>
                                                  </select>             
                                        </div>
                                        <div class="form-group">
                                           <label for="file">File</label>
                                            <input id="file" type="file" name="file" data-parsley-trigger="change" required="" placeholder="Masukan file diterima" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                                               
                                            </div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                    <button class="btn btn-space btn-secondary">Cancel</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
        </div>
      </div>
    </div>
    <!-- End Modal Login -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/libs/js/script_user.js"></script>
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>






