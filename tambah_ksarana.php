<?php
    require_once "static.php";
?>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Tambah Surat Keluar Sarana dan Prasarana</h2>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
            <div class="row">
                        <!-- ============================================================== -->
                        <!-- basic form -->
                        <!-- ============================================================== -->
                        <div class="col-xl-1 col-lg-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Surat Keluar</h5>
                                <div class="card-body">
                                    <form method="post" action="ksarana_insertkel.php" id="basicform" enctype="multipart/form-data" data-parsley-validate="">
                                        <div class="form-group">
                                            <label for="id_sarana">Id surat</label>
                                            <input id="id_sarana" type="number" name="id_sarana" data-parsley-trigger="change" required="" placeholder="Masukan id surat" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_surat">Nama Surat</label>
                                            <input id="nama_surat" type="text" name="nama_surat" data-parsley-trigger="change" required="" placeholder="Masukan nama surat" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="pengirim">Pengirim</label>
                                            <input id="pengirim" type="text" name="pengirim" data-parsley-trigger="change" required="" placeholder="Masukan nama pengirim" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="penerima">Penerima</label>
                                            <input id="penerima" type="text" name="penerima" data-parsley-trigger="change" required="" placeholder="Masukan nama penerima" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal">Tanggal Terima</label>
                                            <input id="tanggal" type="date" name="tanggal" data-parsley-trigger="change" required="" placeholder="Masukan Tanggal Terima" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="file">File</label>
                                            <input id="file" type="file" name="file" data-parsley-trigger="change" required="" placeholder="Masukan nama penerima" autocomplete="off" class="form-control" accept="image/x-png,image/gif,image/jpeg">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                                               
                                            </div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                    <button class="btn btn-space btn-secondary">Cancel</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end basic form -->

            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php
    require_once "footer.php";
?>          
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>