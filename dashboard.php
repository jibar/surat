<?php
    session_start();
    $stat = $_SESSION['status'];
    if ($stat=='admin')
        require_once "static.php";
    else
        require_once "static1.php";
?>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pag
                        eheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Pengarsipan Surat </h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">List</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                    <div class="ecommerce-widget">

                           
                                <!-- ============================================================== -->
                                <!-- end top perfomimg  -->
                                <!-- ============================================================== -->
                           
                        <div class="row">
                            <!-- ============================================================== -->
                            <!-- sales  -->
                            <!-- ============================================================== -->
                            <?php include('koneksi.php'); ?>
                            <?php $pilih = "select * from masuk"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                        <h5 class="text-muted">Surat Masuk</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from kurikulum"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Masuk Kurikulum</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from sarana"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Masuk Sarana dan Prasarana</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from kesiswaan"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Masuk Kesiswaan</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from humas"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Masuk Humas</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end sales  -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- new customer  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from keluar"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Keluar</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from kkurikulum"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Keluar Kurikulum</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from ksarana"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Keluar Sarana dan Prasarana</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from kkesiswaan"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Keluar Kesiswaan</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from khumas"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Keluar Humas</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end new customer  -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- visitor  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                        <?php $pilih = "select * from keputusan"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Surat Keputusan</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div>
                                    <div>
                                        <div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end visitor  -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- total orders  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="card border-3 border-top border-top-primary">
                                    <div class="card-body">
                                         <?php $pilih = "select * from user"; 
                          $hasil = mysqli_query($koneksi, $pilih);
                          $jumlah = mysqli_num_rows($hasil);?>
                                        <h5 class="text-muted">Total User</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"><?php echo $jumlah;?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end total orders  -->
                            <!-- ============================================================== -->
                             <!-- ============================================================== -->
                            <!-- total orders  -->
                            <!-- ============================================================== -->
                        <div class="row">
                            <!-- ============================================================== -->
                            <!-- total revenue  -->
                            <!-- ============================================================== -->

            <?php
    require_once "footer.php";
?>          
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>