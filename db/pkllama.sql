-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2019 at 09:48 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl`
--

-- --------------------------------------------------------

--
-- Table structure for table `keluar`
--

CREATE TABLE `keluar` (
  `idk` int(12) NOT NULL,
  `nok` varchar(25) NOT NULL,
  `penerimak` varchar(75) NOT NULL,
  `pengirimk` varchar(50) NOT NULL,
  `tglk` date NOT NULL,
  `perihalk` varchar(100) NOT NULL,
  `filek` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keluar`
--

INSERT INTO `keluar` (`idk`, `nok`, `penerimak`, `pengirimk`, `tglk`, `perihalk`, `filek`) VALUES
(1, '33342', 'Dinas', 'Kantor', '2019-05-15', 'Undangan', 'foto.png'),
(2, '36938649', 'Anggota', 'Kepala', '2019-04-05', 'Undangan', 'user.png');

-- --------------------------------------------------------

--
-- Table structure for table `keputusan`
--

CREATE TABLE `keputusan` (
  `idkp` int(12) NOT NULL,
  `nokp` varchar(25) NOT NULL,
  `namakp` varchar(100) NOT NULL,
  `tglkp` date NOT NULL,
  `detilkp` varchar(150) NOT NULL,
  `filekp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keputusan`
--

INSERT INTO `keputusan` (`idkp`, `nokp`, `namakp`, `tglkp`, `detilkp`, `filekp`) VALUES
(3, '937497923', 'Presiden', '2019-09-10', '', 'user.png'),
(4, '834', 'Joko', '2019-06-04', 'apa aja', 'WhatsApp Image 2019-04-20 at 13.47.28.jpeg'),
(5, '98738692', 'budi', '2019-10-10', 'Nothing', 'koi.PNG'),
(6, '3702439', 'Surat', '2019-01-01', 'Terserah', 'mi.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `masuk`
--

CREATE TABLE `masuk` (
  `idm` int(12) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `penerimam` varchar(75) NOT NULL,
  `pengirimm` varchar(50) NOT NULL,
  `tglm` date NOT NULL,
  `perihalm` varchar(100) NOT NULL,
  `filem` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masuk`
--

INSERT INTO `masuk` (`idm`, `nom`, `penerimam`, `pengirimm`, `tglm`, `perihalm`, `filem`) VALUES
(4, '082319922991', 'Za', 'Reza', '2019-10-10', 'Delegasi', 'a2.PNG'),
(9, '38462938', 'Pusat', 'Kantor', '2019-05-31', 'Delegasi', 'user.png'),
(10, '8497283', 'kepala', 'hati', '2019-05-28', 'Seminar', 'user.png'),
(11, '3487239834', 'Presiden', 'Menteri', '2019-05-27', 'Rapat', 'user.png'),
(12, '9376589', 'DPR', 'DPRD', '2019-05-26', 'Kebijakan', 'user.png'),
(13, '9943629', 'Arthur', 'Holmes', '2019-05-17', 'Kasus Baru', 'user.png'),
(15, '49659326029', 'L Lawliet', 'NEAR', '2019-05-17', 'Death Note', 'user.png'),
(16, '69374682985', 'Natsu', 'Gray', '2019-05-12', 'Pertarungan', 'user.png'),
(17, '8649283627', 'Luffy', 'Trafalgar', '2019-05-15', 'Samudra Baru', 'user.png'),
(18, '47328649273', 'Tanah', 'Awan', '2019-05-02', 'Hujan', 'user.png'),
(19, '35r434233', 'ADA', 'ADA', '2019-09-09', 'Tidak Ada', 'koi.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(7) NOT NULL,
  `nama` varchar(75) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `alamat` varchar(75) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `level` varchar(15) NOT NULL,
  `fotou` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `pass`, `alamat`, `nohp`, `level`, `fotou`) VALUES
(2, 'user', 'user', 'Jakarta', '083928374268', 'user', 'user.png'),
(3, 'A', 'A', 'AS', '123456566454', 'user', 'koi.PNG'),
(4, 'User3', 'user3', 'Selong', '0812348378223', 'user', 'koi.PNG'),
(5, 'Admin', 'admin', 'Mataram', '081234567890', 'admin', 'user.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keluar`
--
ALTER TABLE `keluar`
  ADD PRIMARY KEY (`idk`);

--
-- Indexes for table `keputusan`
--
ALTER TABLE `keputusan`
  ADD PRIMARY KEY (`idkp`);

--
-- Indexes for table `masuk`
--
ALTER TABLE `masuk`
  ADD PRIMARY KEY (`idm`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keluar`
--
ALTER TABLE `keluar`
  MODIFY `idk` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `keputusan`
--
ALTER TABLE `keputusan`
  MODIFY `idkp` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `masuk`
--
ALTER TABLE `masuk`
  MODIFY `idm` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
