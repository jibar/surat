<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
</head>

<body>
    <?php
  include 'koneksi.php';
    if($_GET['cari']) {
        // mengambil data berdasarkan id
        if ($_GET['cari']=="") {
            header("location: keluar_sarana.php?halaman=1") ;    
        }
            else {
        $sql = "SELECT * FROM ksarana WHERE id_sarana LIKE '%".$_GET["cari"]."%' or nama_surat LIKE '%".$_GET["cari"]."%' or pengirim LIKE '%".$_GET["cari"]."%' or penerima LIKE '%".$_GET["cari"]."%' order by tanggal ASC";
        }
        $hasil= mysqli_query($koneksi, $sql);
         if (mysqli_num_rows($hasil)>0){
       ?>
             <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Id masuk</th>
                                                <th>Nama Surat</th>
                                                <th>Pengirim</th>
                                                <th>Penerima</th>
                                                <th>Tanggal</th>
                                                <th>File</th>
                                                <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody><?php 
                                               
                                            $no=1;
                                            while($data = mysqli_fetch_array($hasil)){
                                                ?>
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $data["id_sarana"];?></td>
                                                <td><?php echo $data["nama_surat"];?></td>
                                                <td><?php echo $data["pengirim"];?></td>
                                                <td><?php echo $data["penerima"];?></td>
                                                <td><?php echo $data["tanggal"];?></td>
                                                <td><img src="file/<?php echo $data["file"];?>"></td>
                                                <td><button type="button" class="btn-primary" data-toggle="modal" data-target="#login">
                                                  Edit
                                                </button></td>
                                                <td><a href="hapus_ksarana.php?id=<?php echo $data["id_sarana"]; ?>">hapus</a></td>
                                            </tr>
                                            <?php $no++; } } else echo "<center><h3>Data Tidak Ditemukan</h3></center>"; ?> 
                                            
                                    </table>
                                </div>
                            </div>
                        </div>
        <?php 
 
        }
    $koneksi->close();
?>

                <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <div class="card-body">
                        <form method="post" action="#" id="basicform" enctype="multipart/form-data" data-parsley-validate="">
                                        <div class="form-group">
                                            <label for="id_sarana">Id Surat</label>
                                            <input id="id_sarana" type="text" name="id_sarana" data-parsley-trigger="change" required="" value="" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_surat">Nama Surat</label>
                                            <input id="nama_surat" type="text" name="nama_surat" data-parsley-trigger="change" required="" placeholder="Masukan nama surat" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="pengirim">pengirim</label>
                                            <input id="pengirim" type="text" name="pengirim" data-parsley-trigger="change" required="" placeholder="Masukan nama pengirim" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="penerima">Penerima</label>
                                            <input id="penerima" type="text" name="penerima" data-parsley-trigger="change" required="" placeholder="Masukan nama penerima" autocomplete="off" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label for="tanggal">Tanggal</label>
                                            <input id="tanggal" type="date" name="tanggal" data-parsley-trigger="change" required="" placeholder="Masukan tanggal penerima" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="file">File</label>
                                            <input id="file" type="file" name="file" data-parsley-trigger="change" required="" placeholder="Masukan file diterima" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                                               
                                            </div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                    <button class="btn btn-space btn-secondary">Cancel</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
        </div>
      </div>
    </div>
    <!-- End Modal Login -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/libs/js/script_cari_ksarana.js"></script>
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>






