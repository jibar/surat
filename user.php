<?php
    require_once "static.php";
?>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <div class="row">
                         <?php 
                         $batas=10; //satu halaman menampilkan 10 baris
                    
                    $posisi=null;
                    if(empty($halaman)){
                      $posisi=0;
                      $halaman=1;
                    }else{
                      $posisi=($halaman-1)* $batas;
                    }
                     $pilih = "select * from user limit $posisi,$batas "; 
                          $hasil = mysqli_query($koneksi, $pilih);
                    ?>
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                             <nav class="navbar navbar-light bg-light justify-content-between">
                          <a class="navbar-brand">Daftar User</a>
                          <form class="form-inline" method="post" action="cetaku.php">
                            <a> Cari </a>
                            <input class="form-control mr-sm-2" name="cari" id="cari" type="search" placeholder="Search" aria-label="Search">
                          </form>
                        </nav>
                        <div id="container">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>No Handphone</th>
                                                <th>Jabatan</th>
                                                <th>Foto</th>
                                                <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $no=1;
                                            while($data = mysqli_fetch_array($hasil)){
                                                ?>
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $data["nama"];?></td>
                                                <td><?php echo $data["alamat"];?></td>
                                                <td><?php echo $data["nohp"];?></td>
                                                <td><?php echo $data["level"];?></td>
                                                <td><a href="File/<?php echo $data["fotou"];?>"><img src="File/<?php echo $data["fotou"];?>"></a></td>
                                                <td><a class="btn-primary" href="edit_user.php?id=<?php echo $data['id']; ?>">
                                                  Edit
                                                </a></td>
                                                <td><a href="hapusu.php?id=<?php echo $data['id']; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a></td>
                                            </tr>
                                            <?php $no++; } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
                 <?php    
     
// 4). code untuk Menampilkan nomor paging di bagian bawah tabel.
      $sql_paging = mysqli_query($koneksi, "select * from user");
      $jmldata = mysqli_num_rows($sql_paging);
      $jumlah_halaman = ceil($jmldata / $batas);
 
      echo "Halaman :";
      for($i = 1; $i <= $jumlah_halaman; $i++)
        if($i != $halaman) {
          echo "<a href=keputusan.php?halaman=$i>$i</a>|";
        } else {
          echo "<b>$i</b>|";
        }
      ?>
    <br>
    Jumlah data :<?php echo $jmldata;?>
                        <div class="row">
                            <!-- ============================================================== -->
                            <!-- total revenue  -->
                            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer bg-dark fixed-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                             SMA NEGERI 5 MATARAM
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
     <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <div class="card-body">
                        <form method="post" action="#" id="basicform" enctype="multipart/form-data" data-parsley-validate="">
                                        <div class="form-group">
                                            <label for="nama">Nama</label>
                                            <input id="nama" type="text" name="nama" data-parsley-trigger="change" required="" value="" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="ala">Alamat</label>
                                            <input id="ala" type="text" name="ala" data-parsley-trigger="change" required="" placeholder="Masukan nama pengirim" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                           <label for="nohp">No Handphone</label>
                                            <input id="nohp" type="number" name="nohp" data-parsley-trigger="change" required="" placeholder="Masukan no hp" autocomplete="off" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="level">State</label>
                                                  <select id="level" name="level" class="form-control">
                                                    <option selected value=0>Pilih</option>
                                                    <option value="admin">Admin</option>
                                                    <option value="user">User</option>
                                                  </select>             
                                        </div>
                                        <div class="form-group">
                                           <label for="file">File</label>
                                            <input id="file" type="file" name="file" data-parsley-trigger="change" required="" placeholder="Masukan nama penerima" autocomplete="off" class="form-control" accept="image/x-png,image/gif,image/jpeg">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
                                               
                                            </div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                    <a class="btn btn-space btn-secondary">Cancel</a>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
        </div>
      </div>
    </div>
    <!-- End Modal Login -->

    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/libs/js/script_user.js"></script>
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>