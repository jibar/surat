<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Dokumen</title>
</head>

<body>
    <?php
  include 'koneksi.php';
    if($_GET['cari']) {
        // mengambil data berdasarkan id
        if ($_GET['cari']=="") {
            header("location: masuk.php?halaman=1") ;    
        }
            else {
        $sql = "SELECT * FROM dokumen WHERE namadok LIKE '%".$_GET["cari"]."%' or tgldok LIKE '%".$_GET["cari"]."%' or ketdok LIKE '%".$_GET["cari"]."%' order by tgldok ASC";
        }

        $hasil= mysqli_query($koneksi, $sql);
         if (mysqli_num_rows($hasil)>0){
       ?>
             <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                               <th>No</th>
                                                <th>Nama Dokumen</th>
                                                <th>Tanggal</th>
                                                <th>Keterangan</th>
                                                 <th>Detail</th>
                                                 <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody><?php 
                                               
                                            $no=1;
                                            while($data = mysqli_fetch_array($hasil)){
                                                ?>
                                            <tr>
                                                <th><?php echo $no;?></th>
                                            <th><?php echo $data["namadok"];?></th>
                                            <th><?php echo $data["tgldok"];?></th>
                                            <th><?php echo $data["ketdok"];?></th>
                                            <th><a href="#">Lihat</a></th>
                                            <td><button type="button" class="btn-primary view_data" onclick="window.location.href='detil_dok.php?id=<?php echo $data['iddok']; ?>'">
                                                  Lihat
                                                </button></td>
                                                <td><a href="hapusdok.php?id=<?php echo $data['iddok']; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a></td>
                                            </tr>
                                            <?php $no++; } } else echo "<center><h3>Data Tidak Ditemukan</h3></center>"; ?> 
                                            
                                    </table>
                                </div>
                            </div>
                        </div>
        <?php 
 
        }
    $koneksi->close();
?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/libs/js/script_cari.js"></script>
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>






