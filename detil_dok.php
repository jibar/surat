<?php
    require_once "static.php";
    $nos = $_GET['id'];
?>

 <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Dokumen</h2>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                           <!-- content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- profile -->
                        <!-- ============================================================== -->
                        <div class="col-xl-1 col-lg-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-5 col-sm-12 col-12">
                            <!-- ============================================================== -->
                            <!-- card profile -->
                            <!-- ============================================================== -->
                            <?php include 'koneksi.php';
                                $id = $_SESSION['id'];
                                $que= "select * from dokumen where iddok = $nos";
                                
                                $hasil= mysqli_query($koneksi,$que);
                                while ($data=mysqli_fetch_array($hasil)) {
                            ?>

                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center">
                                        <h2 class="font-24 mb-0"><?php echo $data['namadok']; ?></h2>
                                        <p></p>
                                    </div>
                                </div>
                                <div class="card-body border-top">
                                    <h3 class="font-16">Detil</h3>
                                    <div class="">
                                        <ul class="list-unstyled mb-0">
                                        <li class="mb-0">Tanggal : <?php echo $data['tgldok']; ?></li>
                                        <li class="mb-2">Keterangan : </i><?php echo $data['ketdok']; ?></li>
                                
                                    </ul>
                                    <button onclick="window.location.href='edit_dok.php?id=<?php echo $nos ?>'" class="btn-primary mr-auto">Edit</button>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end card profile -->
                            <!-- ============================================================== -->
                            <label for="file">File SPK</label>
                            	<form method="post" action="file_spk.php?no=<?php echo $nos;?>&j='spk'" enctype="multipart/form-data">
                                            <input id="file" type="file" name="file[]" data-parsley-trigger="change" required=""  autocomplete="off" class="form-control" multiple>
                                            <button class="btn-primary ml-auto"> Simpan </button>
                                </form>
                            <table class="table table-striped table-bordered first">
                                        <tbody>
                                        	<thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Dokumen</th>
                                                 <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php
                                            		$no=1;
                                            		$que = "select * from berkas where nodok = $nos and jenis = 'spk' ";
                                            		 $hasil = mysqli_query($koneksi,$que);
                                            		 while ($data = mysqli_fetch_array($hasil)) {
                                            	 ?>
                                            <tr>
                                            	 <td><?php echo $no; ?></td>
                                                <td ><a href="lihat.php?lihat=uploads/<?php echo $data['namber']; ?>"><?php echo $data['namber']; ?></a>
                                                </td>
                                                <td>
                                                	<a href="hapusfot.php?id=<?php echo $data['idber']; ?>&no=<?php echo $nos; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a>
                                                </td>
                                            </tr>
                                            <?php $no++; } ?>
                                        </tbody>
                                    </table>
                               <label for="file">File Termin</label>
                               		<form method="post" action="file_spk.php?no=<?php echo $nos;?>&j='termin'" enctype="multipart/form-data">
                                            <input id="file" type="file" name="file[]" data-parsley-trigger="change" required="" placeholder="Masukan nama penerima" autocomplete="off" class="form-control" multiple>
                                            <button class="btn-primary ml-auto"> Simpan </button>
                                    </form>
                                    <table class="table table-striped table-bordered first">
                                        <tbody>
                                        	<thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Dokumen</th>
                                                 <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php
                                            		$no=1;
                                            		$que = "select * from berkas where nodok = $nos and jenis = 'termin' ";
                                            		 $hasil = mysqli_query($koneksi,$que);
                                            		 while ($data = mysqli_fetch_array($hasil)) {
                                            	 ?>
                                            <tr>
                                            	 <td><?php echo $no; ?></td>
                                                <td ><a href="lihat.php?lihat=uploads/<?php echo $data['namber']; ?>"><?php echo $data['namber']; ?></a>
                                                </td>
                                                <td>
                                                	<a href="hapusfot.php?id=<?php echo $data['idber']; ?>&no=<?php echo $nos; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a>
                                                </td>
                                            
                                            </tr>
                                            <?php $no++; } ?>
                                        </tbody>
                                    </table>
                               <label for="file">Laporan Mingguna/Bulanan</label>
                                  		<form method="post" action="file_spk.php?no=<?php echo $nos;?>&j='laporan'" enctype="multipart/form-data">
                                            <input id="file" type="file" name="file[]" data-parsley-trigger="change" required=""  autocomplete="off" class="form-control" multiple>
                                            <button class="btn-primary ml-auto"> Simpan </button>
                                </form>
                                <table>
                            <tbody>
                                        	<thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Dokumen</th>
                                                 <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php
                                            		$no=1;
                                            		$que = "select * from berkas where nodok = $nos and jenis = 'laporan' ";
                                            		 $hasil = mysqli_query($koneksi,$que);
                                            		 while ($data = mysqli_fetch_array($hasil)) {
                                            	 ?>
                                            <tr>
                                            	
                                            	 <td><?php echo $no; ?></td>
                                                <td ><a href="lihat.php?lihat=uploads/<?php echo $data['laporan']; ?>"><?php echo $data['namber']; ?></a>
                                                </td>
                                                <td>
                                                	<a href="hapusfot.php?id=<?php echo $data['idber']; ?>&no=<?php echo $nos; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a>
                                                </td>
                                              </tr>
                                              <?php $no++; } ?>

                                        </tbody>
                                    </table>
                               <label for="file">File Gambar</label>
                                    	<form method="post" action="file_spk.php?no=<?php echo $nos;?>&j='gambar'" enctype="multipart/form-data">
                                            <input id="file" type="file" name="file[]" data-parsley-trigger="change" required=""  autocomplete="off" class="form-control" multiple>
                                            <button class="btn-primary ml-auto"> Simpan </button>
                                </form>
                                <table>
                            <tbody>
                                        	<thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Dokumen</th>
                                                 <th colspan="2">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php
                                            		$no=1;
                                            		$que = "select * from berkas where nodok = $nos and jenis = 'gambar' ";
                                            		 $hasil = mysqli_query($koneksi,$que);
                                            		 while ($data = mysqli_fetch_array($hasil)) {
                                            	 ?>
                                            <tr>
                                            	 <td><?php echo $no; ?></td>
                                                <td ><a href="lihat.php?lihat=uploads/<?php echo $data['namber']; ?>"><?php echo $data['namber']; ?></a>
                                                </td>
                                                <td>
                                                	<a href="hapusfot.php?id=<?php echo $data['idber']; ?>&no=<?php echo $nos; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a>
                                                </td>
                                            </tr>
                                             <?php $no++; } ?>
                                        </tbody>
                                    </table>
                               <label for="file">File Lain - Lain</label>
                                    	<form method="post" action="file_spk.php?no=<?php echo $nos;?>&j='lain'" enctype="multipart/form-data">
                                            <input id="file" type="file" name="file[]" data-parsley-trigger="change" required=""  autocomplete="off" class="form-control" multiple>
                                            <button class="btn-primary ml-auto"> Simpan </button>
                                </form>
                                <tbody>
                                	<thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Dokumen</th>
                                         <th colspan="2">Aksi</th>
                                    </tr>
                                </thead>
                                <table>
                                <tbody>
                                    <tr>
                                    	<?php
                                    		$no=1;
                                    		$que = "select * from berkas where nodok = $nos and jenis = 'lain' ";
                                    		 $hasil = mysqli_query($koneksi,$que);
                                    		 while ($data = mysqli_fetch_array($hasil)) {
                                    	 ?>
                                    	 <td><?php echo $no; ?></td>
                                        <td ><a href="lihat.php?lihat=uploads/<?php echo $data['namber']; ?>"><?php echo $data['namber']; ?></a>
                                        </td>
                                        <td>
                                        	<a href="hapusfot.php?id=<?php echo $data['idber']; ?>&no=<?php echo $nos; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')">[Hapus]</a>
                                        </td>
                                    <?php $no++; } ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end profile -->
                        <!-- ============================================================== -->
                <?php } ?>
                 <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer bg-dark fixed-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                             Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>

