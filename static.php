<!doctype html>
<html lang="en">
 <?php session_start(); if(!isset($_SESSION['nama'])) 
    header("location: index.php");
    include 'koneksi.php';
  ?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
     <link rel="stylesheet" href="assets/libs/css/dropzone.css">
  <script src="assets/libs/js/dropzone.js"></script>
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Pengarsipan Surat</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-primary fixed-top">
                <a class="navbar-brand" href="#"><img style="height: 60px; width: 140px;" src="file/logo.jpg"></a>
       <!--  <a href="dashboard.php"> Dinas Pekerjaan Umum Kota Mataram</a> -->     
        <!-- <a class="navbar-brand" href="index.php">DPUPR Kota Mataram</a>    -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="file/<?php $foto = $_SESSION['foto']; echo $foto  ?>" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name"><?php  
                                    $nama = $_SESSION['nama']; echo $nama  ?></h5>
                                    <span class="status"></span><span class="ml-2">Available</span>
                                </div>
                                <a class="dropdown-item" href="profil.php"><i class="fas fa-user mr-2"></i>Account</a>
                                 <?php $id = $_SESSION['id']; ?>
                                <a class="dropdown-item" href="ganti.php?id=<?php echo $id; ?>"><i class="fas fa-cog mr-2"></i>Ganti Password</a>
                                <a class="dropdown-item" href="logout.php" onclick="return confirm('Anda yakin akan keluar ?')"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            <li class="nav-item ">
                                <a  href="dashboard.php" ><i class="fa fa-fw fa-home"></i>Dashboard </a>
                               
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fa-fw  fa-object-group"></i>Surat Masuk</a>
                                <div id="submenu-2" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_masuk.php">Tambah Surat</a>
                                            </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="masuk.php?halaman=1">Daftar Surat</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_kurikulum.php">Tambah Surat Kurikulum</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="masuk_kurikulum.php?halaman=1">Daftar Surat Kurikulum</a>
                                        </li> 
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_sarana.php">Tambah Surat Sarana dan Prasarana</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="masuk_sarana.php?halaman=1">Daftar Surat Sarana dan Prasarana</a>
                                        </li> 
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_kesiswaan.php">Tambah Surat Kesiswaan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="masuk_kesiswaan.php?halaman=1">Daftar Surat Kesiswaan</a>
                                        </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="tambah_humas.php">Tambah Surat Humas</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="masuk_humas.php?halaman=1">Daftar Surat Humas</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw  fa-object-group"></i>Surat keluar</a>
                                <div id="submenu-3" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_keluar.php">Tambah Surat</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="Keluar.php?halaman=1">Daftar Surat</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_kkurikulum.php">Tambah Surat Kurikulum</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="keluar_kurikulum.php?halaman=1">Daftar Surat Kurikulum</a>
                                        </li> 
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_ksarana.php">Tambah Surat Sarana dan Prasarana</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="keluar_sarana.php?halaman=1">Daftar Surat Sarana dan Prasarana</a>
                                        </li> 
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_kkesiswaan.php">Tambah Surat Kesiswaan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="keluar_kesiswaan.php?halaman=1">Daftar Surat Kesiswaan</a>
                                        </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="tambah_khumas.php">Tambah Surat Humas</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="keluar_humas.php?halaman=1">Daftar Surat Humas</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4"><i class="fab fa-fw fa-wpforms"></i>Surat Keputusan</a>
                                <div id="submenu-4" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_keputusan.php">Tambah Surat</a>
                                        </li>
                                        <li class="nav-item">
                                           <a class="nav-link" href="keputusan.php?halaman=1"> Daftar Surat</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- <li class="nav-item ">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-4"><i class="fab fa-fw fa-wpforms"></i>Dokumen</a>
                                <div id="submenu-5" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="dokumen.php?halaman=1"> Daftar Dokumen</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_dokumen.php">Tambah Dokumen</a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-5"><i class="fa fa-fw fa-user-circle"></i>User</a>
                                <div id="submenu-6" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="tambah_user.php">Tambah User</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="user.php?halaman=1">Daftar User</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
      